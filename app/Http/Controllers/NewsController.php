<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;
use Validator;
use Input;

class NewsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $news = News::all();
//        return $news;
        return view('news.index', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $news = new News();
        return view('news.add', compact('news'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
//        return $request;
        $data = $request->except(['_token']);

        $v = Validator::make($data, [
                    'title' => 'required',
                    'description' => 'required',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }

        $news = News::create($data);
//        return $data['news_videos'];
        if (!empty($data['news_videos'])) {
            foreach ($data['news_videos'] as $video) {
                $video_data = ['news_id' => $news->id, 'url' => $video['url']];
//                print_r($video_data);       
                $news->news_videos()->create($video_data);
            }
        }
//        $news->news_videos()->create($data['news_videos']);
//        exit;



        return redirect('news')->with('message', 'News Saved Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news) {
        $news = News::findOrFail($news);
//        return $news->news_videos;
        return view('news.add', compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news) {
        $data = $request->except(['_token']);

        $v = Validator::make($data, [
                    'title' => 'required',
                    'description' => 'required',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }


        $news->update($data);
        $all_array = [];
        if (!empty($news->news_videos)) {
            foreach ($news->news_videos as $item) {
                $all_array[] = $item->id;
            }
        }

        $array_diff = [];
        if (!empty($data['news_videos'])) {
            foreach ($data['news_videos'] as $video) {
                if (isset($video['id']))
                    $array_diff[] = $video['id'];
            }
        }
        $array_to_delete = array_diff($all_array, $array_diff);
        if (!empty($array_to_delete)) {
            $news->news_videos()->whereIn('id', $array_to_delete)->delete();
        }

        if (!empty($data['news_videos'])) {
            foreach ($data['news_videos'] as $video) {
                $video_data = ['news_id' => $news->id, 'url' => $video['url']];
                if (!empty($video['id'])) {
                    $news_videos = \App\Models\NewsVideos::find($video['id']);
                    $news_videos->update($video_data);
                } else {
                    $news->news_videos()->create($video_data);
                }
            }
        }

        return redirect('news')->with('message', 'News Saved Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news) {
        $item = News::findOrFail($news);
        $item->delete();

        return redirect('news.index');
    }

}

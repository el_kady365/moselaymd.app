<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsVideos extends Model {

    //
    protected $fillable = ['url'];

    public function news() {
        return $this->belongsTo('App\Models\News');
    }

    public function getEmbedAttribute() {
//        var_dump($this->attributes);
        return preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i", "<iframe width=\"420\" height=\"315\" src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" allowfullscreen></iframe>", $this->attributes['url']);
    }
}

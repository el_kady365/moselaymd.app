@extends('master')
@section('content')


<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">List All News</h3>
            </div>
            <div class="panel-body">
                <div class="actions">
                    <a href="{{url('news/create')}}" class="btn btn-primary">Add News Item</a>
                </div>
                <div class="table-responsive ls-table">
                    <table class="table table-bordered table-striped table-bottomless">
                        <thead>
                            <tr><th>#</th>
                                <th>Title</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($news as $news)
                            <tr data-id="{{$news->id}}">

                                <td>{{$news->id}}</td>
                                <td>{{$news->title}}</td>

                                <td>

                                    <a href="{{url('news/'.$news->id.'/edit')}}" class="btn btn-sm btn-info tooltipLink" data-toggle="tooltip" data-placement="top">
                                        <i class="fa fa-edit"></i>
                                    </a>

                                    <a href="{{url('news/'.$news->id)}}"data-method="delete" 
                                       data-token="{{csrf_token()}}" data-confirm="Are you sure?"  class="btn btn-sm btn-danger tooltipLink" data-toggle="tooltip" data-placement="top" >
                                        <i class="fa fa-trash"></i>
                                    </a>


                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="{{asset('js/laravel.js')}}"></script>
@endsection
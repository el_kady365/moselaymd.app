@extends('master')
@section('css')
<style type="text/css">
    .url-div{
        /*border: 1px dotted #999999;*/
        /*padding-right: 10px;*/
        /*margin:8px;*/
        /*position: relative;*/
    }
    #Options{
        /*border: 1px dotted #999999;*/

        margin-bottom: 30px ;
    }
/*    .delete-section2{
        position: absolute;
        top:0;
        left:0;
        width:16px;
    }*/
    .inline-labeles label
    {
        display: inline-block;
    }
</style>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <h3 class="ls-top-header">
            @if(empty($news->id))
            Add News
            @else
            Edit News
            @endif
        </h3>


    </div>
</div>

<div class="row">
    <div class="col-md-12">

        @if(Session::has('message'))
        <div class="alert alert-success">{!!Session::get('message')!!}</div>
        @endif

        @if(count($errors->all()))
        <div class="alert alert-danger">
            These Errors has occurred:<br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    @if(empty($news->id))
                    Add News
                    @else
                    Edit News
                    @endif
                </h3>
            </div>


            <?php
            if (empty($news->id)) {

                $url = url('news');
            } else {
                $url = url('news/' . $news->id);
            }
            ?>
            <div class="panel-body">
                <form method="post" action="{{$url}}" class="form-horizontal ls_form" enctype="multipart/form-data">
                    <input name="_token" type="hidden" value="{{csrf_token()}}">
                    @if (!empty($news->id))
                    {{ method_field('PUT') }}
                    @endif
                    <div class="row ls_divider ">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Title</label>
                            <div class="col-md-10">

                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <input type="text" name="title" class="form-control ls-group-input" value="{{Input::old('title',$news->title)}}">
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="row ls_divider ">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Content</label>
                            <div class="col-md-10">

                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <textarea name="description"  class="form-control ls-group-input" >{{Input::old('description',$news->description)}}</textarea>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                Videos
                            </h4>
                        </div>
                        <div class="panel-body">
                            <div id="options-div">

                                <div id="Options" class="row">
                                    <div class="col-md-12">
                                        <?php
                                        $section_count = 0;
//                debug($news);


                                        if ($news->news_videos->isNotEmpty()) {

                                            foreach ($news->news_videos as $j => $option) {
                                                //debug($level);
                                                ?>
                                                <div id='row<?php echo $j ?>' class="url-div">
                                                    <div class="row">
                                                        <div class="col-md-11">
                                                            {!! Form::hidden('news_videos[' . $j . '][id]',$option->id, ['class' => 'form-control']) !!}
                                                            {!! Form::label('news_videos[' . $j . '][url]','Video Url') !!}
                                                            {!! Form::text('news_videos[' . $j . '][url]',$option->url, ['class' => 'form-control']) !!}
                                                        </div>
                                                        <div class="col-md-1">
                                                            <a href = '#' class = "delete-section2 btn btn-danger btn-small">
                                                                <i class="fa fa-trash-o"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                                $section_count++;
                                            }
                                        } else {
                                            ?>
                                            <div id='row0' class="url-div">
                                                <div class="row">
                                                    <div class="col-md-11">
                                                        {!! Form::label('news_videos[0][url]','Video Url') !!}
                                                        {!! Form::text('news_videos[0][url]','',['class' => 'form-control']) !!}
                                                    </div>
                                                    <div class="col-md-1">
                                                        <a href = '#' class = "delete-section2 btn btn-danger btn-small">
                                                            <i class="fa fa-trash-o"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>

                                    </div>

                                </div>
                                <a href="#" class="add-video btn btn-xs btn-primary">
                                    <i class="fa fa-plus-circle"></i> <?php echo __('Add Video') ?>
                                </a>
                            </div>
                        </div>
                    </div>








                    <div class="row ls_divider last">
                        <div class="form-group">
                            <label class="col-md-2 control-label"></label>
                            <div class="col-sm-10">

                                <input class="submit btn-primary btn" type="submit" name="submit" value="Save">

                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $(function () {
        $('.add-video').on('click', function () {
            if ($('#Options').is(':visible')) {
                $('#Options > div.col-md-12').append('<div class="url-div">' + $('.url-div:first').html().replace(/\[0\]/g, '[' + $('.url-div').length + ']').replace(/-0-/g, '-' + $('.url-div').length + '-') + '</div>');
                $('#Options .url-div:last input').val('');
                $("#Options .url-div:last .badge").remove();
                $('#Options .url-div:last textarea').val('');
                $('#Options .url-div:last .asset-type').trigger('change');
            } else {
                $('#Options').show();
            }
            return false;
        });

    });
    $(document).on('click', '.delete-section2', function () {
        if (confirm('<?php echo __('Are you sure you want to delete this item?') ?>')) {
            if ($('#Options').find('.url-div').length <= 1) {
                $(this).closest('.url-div').find('input').val('');

            } else {
                $(this).closest('.url-div').remove();
            }
        }
        return false;
    });
</script>
@endsection
